from django.views import generic

class Base(generic.TemplateView):
    template_name = "index.html"

index = Base.as_view()

class Terms(generic.TemplateView):
    template_name="terms.html"

terms = Terms.as_view()
